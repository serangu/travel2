# Наследуемся от CentOS 7
FROM ubuntu:16.04

# Выбираем рабочую папку
WORKDIR /root

# Устанавливаем wget и скачиваем Go
RUN apt-get update && apt-get install -y wget git unzip && \
    wget https://storage.googleapis.com/golang/go1.9.linux-amd64.tar.gz

# Устанавливаем Go, создаем workspace и папку проекта
RUN tar -C /usr/local -xzf go1.9.linux-amd64.tar.gz && \
    mkdir go && mkdir go/src && mkdir go/bin && mkdir go/pkg && \
    mkdir go/src/dumb && mkdir travel

# Задаем переменные окружения для работы Go
ENV PATH=${PATH}:/usr/local/go/bin GOROOT=/usr/local/go GOPATH=/root/go

# Необходимые библиотеки
RUN go get github.com/julienschmidt/httprouter

# Копируем наш исходный main.go внутрь контейнера, в папку go/src/dumb
ADD main.go go/src/dumb

# Монтируем каталог с данными и разархифируем его
VOLUME /tmp
#RUN unzip /tmp/data/data.zip -d /travel

# Компилируем и устанавливаем наш сервер
RUN go build dumb && go install dumb

# Открываем 80-й порт наружу
EXPOSE 80

# Запускаем наш сервер
CMD ./go/bin/dumb