package main

import (
	"fmt"
	"strconv"
	"net/http"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	s "strings"
	"encoding/json"
	"archive/zip"
	"io"
	"os"
	"path/filepath"
	"bufio"
	_"runtime/debug"
	"runtime"
	"math"
)

// Для округления
func Round(f float64) float64 {
	return math.Floor(f + .5)
}

func RoundPlus(f float64, places int) (float64) {
	shift := math.Pow(10, float64(places))
	return Round(f * shift) / shift;
}

// Структура для получения средней оценки по локации
type Location_full struct {
	visited_at int
	age int
	mark float64
}

// Хэш с информацией обо всех оценках локаций
var locations_full_m = map[int][]Location_full{}
var locations_full_f = map[int][]Location_full{}

// Хэши для хранения готовых json
var users_json = map[int][]byte{}

var locations_json = map[int][]byte{}

// Убрал, т.к. не влезает в оперативную память
//var visits_json = map[int][]byte{}

// Хэши для хранения данных о сущностях
//-1:{BirthDate:0, Email:"", FirstName:"", LastName:"", Gender:"m", ID:-1},
var users = map[int]UserJson{}

//-1:{City:"", Country:"", Distance:0, Place:"", ID:-1},
var locations = map[int]LocationJson{}

//-1:{User:0, Location:0, Visited_at:0, Mark:0, ID:-1},
var visits = map[int]VisitJson{}

type UserJson struct {
	BirthDate int `json:"birth_date"`
	Email string `json:"email"`
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
	Gender string `json:"gender"`
	ID int `json:"id"`
}

type LocationJson struct {
	City string `json:"city"`
	Country string `json:"country"`
	Distance int `json:"distance"`
	ID int `json:"id"`
	Place string `json:"place"`
}

type VisitJson struct {
	User int `json:"user"`
	Location int `json:"location"`
	Visited_at int `json:"visited_at"`
	ID int `json:"id"`
	Mark int `json:"mark"`
}

type UsersJson struct {
	Users []struct {
		BirthDate int `json:"birth_date"`
		Email string `json:"email"`
		FirstName string `json:"first_name"`
		LastName string `json:"last_name"`
		Gender string `json:"gender"`
		ID int `json:"id"`
	} `json:"users"`
}

type LocationsJson struct {
	Locations []struct {
		City string `json:"city"`
		Country string `json:"country"`
		Distance int `json:"distance"`
		ID int `json:"id"`
		Place string `json:"place"`
	} `json:"locations"`
}

type VisitsJson struct {
	Visits []struct {
		User int `json:"user"`
		Location int `json:"location"`
		Visited_at int `json:"visited_at"`
		ID int `json:"id"`
		Mark int `json:"mark"`
	} `json:"visits"`
}

var users_from_json UsersJson
var locations_from_json LocationsJson
var visits_from_json VisitsJson

var user_from_json UserJson
var location_from_json LocationJson
var visit_from_json VisitJson

var time_now int

var skobki []byte

func Unzip(src, dest string) error {

    var filenames []string

    r, _ := zip.OpenReader(src)
    defer r.Close()

    for _, f := range r.File {
        rc, _ := f.Open()
        defer rc.Close()

        // Store filename/path for returning and using later on
        fpath := filepath.Join(dest, f.Name)
        filenames = append(filenames, fpath)

        if f.FileInfo().IsDir() {

            // Make Folder
            os.MkdirAll(fpath, os.ModePerm)

        } else {

            // Make File
            var fdir string
            if lastIndex := s.LastIndex(fpath, string(os.PathSeparator)); lastIndex > -1 {
                fdir = fpath[:lastIndex]
            }

            _ = os.MkdirAll(fdir, os.ModePerm)
            
            f, _ := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
            defer f.Close()

            _, _ = io.Copy(f, rc)
        }
    }
    return nil
}

func init() {
	//debug.SetGCPercent(-1) // Отключение сборки мусора
	
	runtime.GOMAXPROCS(8) // Используем 8 ядер
	
	skobki = []byte("{}")
	
	///Unzip("data.zip", "travel")
	///files, _ := ioutil.ReadDir("travel")	
	
	Unzip("/tmp/data/data.zip", "/travel")
	files, _ := ioutil.ReadDir("/travel")
	
	for _, file := range files {
		// Считываем текущее время из options.txt
		file_options, _ := os.Open("options.txt")
		defer file_options.Close()

		scanner := bufio.NewScanner(file_options)
		scanner.Scan()
		time_now, _ := strconv.Atoi(scanner.Text())
		
		fmt.Printf("%s\n", time_now)
		
		// Считываем значения из json-файлов		
		if s.Contains(file.Name(), "users") {
			///bs, _ := ioutil.ReadFile("travel/"+file.Name()) // []byte
			bs, _ := ioutil.ReadFile("/travel/"+file.Name()) // []byte
			
			if err := json.Unmarshal(bs, &users_from_json); err == nil {
				for _, element := range users_from_json.Users {
					element_bs, _ := json.Marshal(element)
					
					users_json[element.ID] = element_bs
					users[element.ID] = element
				}
			}
		} else if s.Contains(file.Name(), "locations") {
			///bs, _ := ioutil.ReadFile("travel/"+file.Name()) // []byte
			bs, _ := ioutil.ReadFile("/travel/"+file.Name()) // []byte
			
			if err := json.Unmarshal(bs, &locations_from_json); err == nil {
				for _, element := range locations_from_json.Locations {
					element_bs, _ := json.Marshal(element)
					
					locations_json[element.ID] = element_bs
					locations[element.ID] = element
				}
			}
		} else if s.Contains(file.Name(), "visits") {
			///bs, _ := ioutil.ReadFile("travel/"+file.Name()) // []byte
			bs, _ := ioutil.ReadFile("/travel/"+file.Name()) // []byte
			
			if err := json.Unmarshal(bs, &visits_from_json); err == nil {
				for _, element := range visits_from_json.Visits {
					visits[element.ID] = element
				}
			}
		}
		fmt.Println(file.Name())
	}
	
	// Генерирование сущностей для поиска средней оценки локации
	for _, v := range visits {
		// Высчитываем возраст пользователя
		v_temp := time_now
		age := 0
		
		for v_temp > users[v.User].BirthDate {
			v_temp -= 31536000
			age += 1
		}
		
		var temp Location_full
		temp = Location_full{visited_at: v.Visited_at, age: age, mark:float64(v.Mark)}
		
		if users[v.User].Gender == "m" {
			locations_full_m[v.Location] = append(locations_full_m[v.Location], temp)
		} else {
			locations_full_f[v.Location] = append(locations_full_f[v.Location], temp)
		}
	}
	
	// Для отладки
	fmt.Println(locations_full_m[100])
}

func UsersHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		http.NotFound(w, r)
	}

	if users_json[id] == nil {
		http.NotFound(w, r)
	} else {
		w.Write(users_json[id])
	}
}

func LocationsHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		http.NotFound(w, r)
	}

	if locations_json[id] == nil {
		http.NotFound(w, r)
	} else {
		w.Write(locations_json[id])
	}
}

func VisitsHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		http.NotFound(w, r)
	}
	
	if visits[id].ID > 0 {
		bs, _ := json.Marshal(visits[id])
		w.Write(bs)
	} else {
		http.NotFound(w, r)
	}
}

// Функция для выноса в го-рутину
func save_user(user_1 map[string]interface{}, id int) {
	var temp UserJson
	
	// Обновляем данные в users
	temp = users[id]
	for k, v := range user_1 {
		if k == "birth_date" {
			temp.BirthDate = int(v.(float64))
		} else if k == "email" {
			temp.Email = v.(string)
		} else if k == "first_name" {
			temp.FirstName = v.(string)
		} else if k == "last_name" {
			temp.LastName = v.(string)
		} else if k == "gender" {
			temp.Gender = v.(string)
		}
	}
	
	users[id] = temp
	
	// Генерируем json в формате []byte
	bs, _ := json.Marshal(temp)
	users_json[id] = bs
}

func save_location(location_1 map[string]interface{}, id int){
	var temp LocationJson
	
	// Обновляем данные в locations
	temp = locations[id]
	for k, v := range location_1 {
		if k == "distance" {
			temp.Distance = int(v.(float64))
		} else if k == "city" {
			temp.City = v.(string)
		} else if k == "country" {
			temp.Country = v.(string)
		} else if k == "place" {
			temp.Place = v.(string)
		}
	}
	
	locations[id] = temp
	
	// Генерируем json в формате []byte
	bs, _ := json.Marshal(temp)
	locations_json[id] = bs
}

func save_visit(visit_1 map[string]interface{}, id int){
	var temp VisitJson
	
	// Обновляем данные в visits
	temp = visits[id]
	for k, v := range visit_1 {
		if k == "user" {
			temp.User = int(v.(float64)) //?
		} else if k == "location" {
			temp.Location = int(v.(float64))
		} else if k == "visited_at" {
			temp.Visited_at = int(v.(float64))
		} else if k == "mark" {
			temp.Mark = int(v.(float64)) //?
		}
	}
	
	visits[id] = temp
}

// Соответствует ли json схеме по обновлению данных у пользователя?
func CheckUserUpdateJson(json_bd []byte, id int) bool {
	correct_names := []string{"birth_date", "email", "first_name", "last_name", "gender"}
	
	var user_1 map[string]interface{}
	var find_k bool
	
	if err := json.Unmarshal(json_bd , &user_1); err == nil {
		// Смотрим чтобы ключи в json были из нужного нам списка
		for k, v := range user_1 {
			if v == nil {
				return false
			}
			
			find_k = false
			for _, v1 := range correct_names {
				if k == v1 {
					find_k = true
					break
				}
			}
			
			if find_k == false {
				return false
			}
		}
		
		// Вызываем го-рутину
		go save_user(user_1, id)
		
		return true
	} else {
		return false
	}
}

// Соответствует ли json схеме по обновлению данных у локации?
func CheckLocationUpdateJson(json_bd []byte, id int) bool {
	correct_names := []string{"city", "country", "distance", "place"}
	
	var location_1 map[string]interface{}
	var find_k bool
	
	if err := json.Unmarshal(json_bd , &location_1); err == nil {
		// Смотрим чтобы ключи в json были из нужного нам списка
		for k, v := range location_1 {
			if v == nil {
				return false
			}
			
			find_k = false
			for _, v1 := range correct_names {
				if k == v1 {
					find_k = true
					break
				}
			}
			
			if find_k == false {
				return false
			}
		}
		
		// Вызываем го-рутину
		go save_location(location_1, id)
		
		return true
	} else {
		return false
	}
}

// Соответствует ли json схеме по обновлению данных у визита?
func CheckVisitUpdateJson(json_bd []byte, id int) bool {
	correct_names := []string{"user", "location", "visited_at", "mark"}
	
	var visit_1 map[string]interface{}
	var find_k bool
	
	if err := json.Unmarshal(json_bd , &visit_1); err == nil {
		// Смотрим чтобы ключи в json были из нужного нам списка
		for k, v := range visit_1 {
			if v == nil {
				return false
			}
			
			find_k = false
			for _, v1 := range correct_names {
				if k == v1 {
					find_k = true
					break
				}
			}
			
			if find_k == false {
				return false
			}
		}
		
		// Вызываем го-рутину
		go save_visit(visit_1, id)
		
		return true
	} else {
		return false
	}
}

func CreateAndUpdateUsersHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if ps.ByName("id") == "new" {
		body, err := ioutil.ReadAll(r.Body) // []byte
		if err != nil {
			http.NotFound(w, r)
		}
		
		if err := json.Unmarshal(body, &user_from_json); err == nil {
			if users_json[user_from_json.ID] == nil {
				users_json[user_from_json.ID] = body
				users[user_from_json.ID] = user_from_json
				
				w.Write(skobki)
			} else {
				http.Error(w, "", http.StatusBadRequest) // TODO: Можно ускорить просто передавая 400?
			}
		} else {
			http.Error(w, "", http.StatusBadRequest)
		}
	} else {
		id, err := strconv.Atoi(ps.ByName("id"))
		if err != nil {
			http.NotFound(w, r)
		}

		if users_json[id] == nil {
			http.NotFound(w, r)
		} else {
			body, err := ioutil.ReadAll(r.Body) // []byte
			if err != nil {
				http.NotFound(w, r)
			} else {
				if CheckUserUpdateJson(body, id) == false {
					http.Error(w, "", http.StatusBadRequest)
				} else {
					w.Write(skobki)
				}
			}
		}
	}
}

func CreateAndUpdateLocationsHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if ps.ByName("id") == "new" {
		body, err := ioutil.ReadAll(r.Body) // []byte
		if err != nil {
			http.NotFound(w, r)
		}
		
		if err := json.Unmarshal(body, &location_from_json); err == nil {
			if locations_json[location_from_json.ID] == nil {
				locations_json[location_from_json.ID] = body
				locations[location_from_json.ID] = location_from_json
				
				w.Write(skobki)
			} else {
				http.Error(w, "", http.StatusBadRequest) // TODO: Можно ускорить просто передавая 400?
			}
		} else {
			http.Error(w, "", http.StatusBadRequest)
		}
	} else {
		id, err := strconv.Atoi(ps.ByName("id"))
		if err != nil {
			http.NotFound(w, r)
		}

		if locations_json[id] == nil {
			http.NotFound(w, r)
		} else {
			body, err := ioutil.ReadAll(r.Body) // []byte
			if err != nil {
				http.NotFound(w, r)
			} else {
				if CheckLocationUpdateJson(body, id) == false {
					http.Error(w, "", http.StatusBadRequest)
				} else {
					w.Write(skobki)
				}
			}
		}
	}
}

func CreateAndUpdateVisitsHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if ps.ByName("id") == "new" {
		body, err := ioutil.ReadAll(r.Body) // []byte
		if err != nil {
			http.NotFound(w, r)
		}
		
		if err := json.Unmarshal(body, &visit_from_json); err == nil {
			if visits[visit_from_json.ID].ID > 0 {
				http.Error(w, "", http.StatusBadRequest) // TODO: Можно ускорить просто передавая 400?
			} else {
				visits[visit_from_json.ID] = visit_from_json
				w.Write(skobki)
			}
		} else {
			http.Error(w, "", http.StatusBadRequest)
		}
	} else {
		id, err := strconv.Atoi(ps.ByName("id"))
		if err != nil {
			http.NotFound(w, r)
		}

		if visits[id].ID > 0 {
			body, err := ioutil.ReadAll(r.Body) // []byte
			if err != nil {
				http.NotFound(w, r)
			} else {
				if CheckVisitUpdateJson(body, id) == false {
					http.Error(w, "", http.StatusBadRequest)
				} else {
					w.Write(skobki)
				}
			}
		} else {
			http.NotFound(w, r)
		}
	}
}

func AVRHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	location_id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		http.NotFound(w, r)
	}
	
	// А есть ли вообще такая локация в данных?
	if locations[location_id].ID > 0 {
		// Проверка параметров на адекватность
		gender := r.FormValue("gender")
		from_date := r.FormValue("fromDate")
		to_date := r.FormValue("toDate")
		from_age := r.FormValue("fromAge")
		to_age := r.FormValue("toAge")
		
		var from_date_int int
		var to_date_int int
		var from_age_int int
		var to_age_int int
		
		var err1 error
		
		if gender != "" && gender != "m" && gender != "f" {
			http.Error(w, "", http.StatusBadRequest)
		}
		
		if from_date != "" {
			from_date_int, err1 = strconv.Atoi(from_date)
			if err1 != nil {
				http.Error(w, "", http.StatusBadRequest)
			}
			
			if from_date != strconv.Itoa(from_date_int) {
				http.Error(w, "", http.StatusBadRequest)
			}
		}
		
		if to_date != "" {
			to_date_int, err1 = strconv.Atoi(to_date)
			if err1 != nil {
				http.Error(w, "", http.StatusBadRequest)
			}
			
			if to_date != strconv.Itoa(to_date_int) {
				http.Error(w, "", http.StatusBadRequest)
			}
		}
		
		if from_age != "" {
			from_age_int, err1 = strconv.Atoi(from_age)
			if err1 != nil {
				http.Error(w, "", http.StatusBadRequest)
			} 
				
			if from_age != strconv.Itoa(from_age_int) {
				http.Error(w, "", http.StatusBadRequest)
			}
		}
		
		if to_age != "" {
			to_age_int, err1 = strconv.Atoi(to_age)
			if err1 != nil {
				http.Error(w, "", http.StatusBadRequest)
			}
			
			if to_age != strconv.Itoa(to_age_int) {
				http.Error(w, "", http.StatusBadRequest)
			}
		}
		
		// Находим все посещения данного места
		sum := 0.0
		count := 0.0
		var must_sum bool
		
		if gender == "" {
			for _, v := range locations_full_m[location_id] {
				must_sum = true
				
				if from_date != "" && v.visited_at <= from_date_int {
					must_sum = false
				}
				
				if must_sum && to_date != "" && v.visited_at >= to_date_int {
					must_sum = false
				}
				
				if must_sum && from_age != "" && from_age_int > v.age {
					must_sum = false
				}
				
				if must_sum && to_age != "" && to_age_int <= v.age {
					must_sum = false
				}
				
				if must_sum {
					sum += v.mark
					count += 1
				}
			}
			
			for _, v := range locations_full_f[location_id] {
				must_sum = true
				
				if from_date != "" && v.visited_at <= from_date_int {
					must_sum = false
				}
				
				if must_sum && to_date != "" && v.visited_at >= to_date_int {
					must_sum = false
				}
				
				if must_sum && from_age != "" && from_age_int > v.age {
					must_sum = false
				}
				
				if must_sum && to_age != "" && to_age_int <= v.age {
					must_sum = false
				}
				
				if must_sum {
					sum += v.mark
					count += 1
				}
			}
		} else if gender == "m" {
			for _, v := range locations_full_m[location_id] {
				must_sum = true
				
				if from_date != "" && v.visited_at <= from_date_int {
					must_sum = false
				}
				
				if must_sum && to_date != "" && v.visited_at >= to_date_int {
					must_sum = false
				}
				
				if must_sum && from_age != "" && from_age_int > v.age {
					must_sum = false
				}
				
				if must_sum && to_age != "" && to_age_int <= v.age {
					must_sum = false
				}
				
				if must_sum {
					sum += v.mark
					count += 1
				}
			}
		} else if gender == "f" {
			for _, v := range locations_full_f[location_id] {
				must_sum = true
				
				if from_date != "" && v.visited_at <= from_date_int {
					must_sum = false
				}
				
				if must_sum && to_date != "" && v.visited_at >= to_date_int {
					must_sum = false
				}
				
				if must_sum && from_age != "" && from_age_int > v.age {
					must_sum = false
				}
				
				if must_sum && to_age != "" && to_age_int <= v.age {
					must_sum = false
				}
				
				if must_sum {
					sum += v.mark
					count += 1
				}
			}
		}
		
		if count == 0 {
			w.Write([]byte(`{"avg": 0}`))
		} else {
			result_str := `{"avg":` + strconv.FormatFloat(RoundPlus(sum/count, 5), 'f', -1, 64) + `}`
			
			w.Write([]byte(result_str))
		}
	} else {
		http.NotFound(w, r)
	}	
}

func main () {
    router := httprouter.New()
    router.GET("/users/:id", UsersHandler)
    router.GET("/locations/:id", LocationsHandler)
    router.GET("/visits/:id", VisitsHandler)
    
    // Добавление/Редактирование новых записей
    router.POST("/users/:id", CreateAndUpdateUsersHandler)
    router.POST("/locations/:id", CreateAndUpdateLocationsHandler)
    router.POST("/visits/:id", CreateAndUpdateVisitsHandler)
       
    // Получение средней оценки локации
    router.GET("/locations/:id/avg", AVRHandler) 
      
    http.ListenAndServe(":80", router)
}